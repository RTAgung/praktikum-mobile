package com.example.tugas3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("Top Youtuber Gaming Indonesia");

        rvList = findViewById(R.id.rv_list);

        ArrayList<YoutubeCh> listData = DataDummy.getTopYtData();
        YoutubeChAdapter adapter = new YoutubeChAdapter();
        adapter.setData(listData);

        rvList.setAdapter(adapter);
        rvList.setLayoutManager(new LinearLayoutManager(this));
    }
}