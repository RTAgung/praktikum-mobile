package com.example.tugas3;

import java.util.ArrayList;

public class DataDummy {
    private static final String[] name = {
            "Jess No Limit",
            "Frost Diamond",
            "MiawAug",
            "Dyland PROS",
            "FrontaL Gaming",
            "BUDI01 GAMING",
            "Rendy Rangers",
            "LetDa Hyper",
            "kemas pake z",
            "Erpan1140"
    };

    private static final String[] subs = {
            "21.5M subscribers",
            "17.6M subscribers",
            "14.1M subscribers",
            "13.9M subscribers",
            "10.6M subscribers",
            "10.5M subscribers",
            "8.16M subscribers",
            "8.13M subscribers",
            "8.06M subscribers",
            "7.36M subscribers"
    };

    private static final String[] photo = {
            "https://yt3.ggpht.com/ytc/AAUvwngcZGn6WEZ5hhDcbP7m_nNva3noclM9w-7WbjGCFQ=s176-c-k-c0x00ffffff-no-rj",
            "https://yt3.ggpht.com/ytc/AAUvwnievx2mFHJYKyCN6YlK-FIENsNBqQuyVGOjYZGIDw=s176-c-k-c0x00ffffff-no-rj",
            "https://yt3.ggpht.com/ytc/AAUvwnjUjV1mChfjDBcI-mk8-5z8u8SFWk7_Emh4g9YlQQ=s176-c-k-c0x00ffffff-no-rj",
            "https://yt3.ggpht.com/ytc/AAUvwnggEMDuOK4NG9JjGuBQEh6Tt5FYkH_az7rOmugmNw=s176-c-k-c0x00ffffff-no-rj",
            "https://yt3.ggpht.com/ytc/AAUvwnhz_MK0puz85Uvs36_SmoliLQoM3p8hHyPCIw9gXA=s176-c-k-c0x00ffffff-no-rj",
            "https://yt3.ggpht.com/ytc/AAUvwni9r4cBM8ZxAr0oZzazcjR70DG9niFE9VyUwk4zUg=s176-c-k-c0x00ffffff-no-rj",
            "https://yt3.ggpht.com/ytc/AAUvwnhtnaL7ZdmYmnNhrNSoDR-8KgrfWlAqcdTFDPiFDQ=s176-c-k-c0x00ffffff-no-rj",
            "https://yt3.ggpht.com/ytc/AAUvwnhexGtEqkbU02nHu4o_jQ4kp3QIWuufHFci8dGO=s176-c-k-c0x00ffffff-no-rj",
            "https://yt3.ggpht.com/ytc/AAUvwniPofofv29TEpl7U1Rpsl1cXonFXPvg9QzEpyxelg=s176-c-k-c0x00ffffff-no-rj",
            "https://yt3.ggpht.com/ytc/AAUvwnh5aTH-VGltCZs6hZVNjfdUT-8wSF3Fr5nA_MaksA=s176-c-k-c0x00ffffff-no-rj"
    };

    private static final String[] description = {
            "Do the best and let God do the rest :)\n" +
                    "\n" +
                    "Instagram : http://instagram.com/JessNoLimit\n" +
                    "TikTok : https://www.tiktok.com/@jessnolimit999\n" +
                    "Twitter : https://twitter.com/jessnolimit999\n" +
                    "\n" +
                    "Business & partnership : \n" +
                    "partnership@jnl-ads.com\n" +
                    "Let's together break the limit!\n" +
                    "\n" +
                    "Thanks for coming!\n",
            "Welcome Back Guys, My Name Is Frost\n" +
                    "\n" +
                    "Disini Adalah Chanel Video Gaming, Vlog, Dll \n" +
                    "Tapi Lebih fokus ke gaming\n" +
                    "\n" +
                    "\n" +
                    "Jangan Lupa Tinggalin Like dan komentar\n" +
                    "Dan jangan lupa juga share, Subscribe GRATIS!!!\n" +
                    "Karna Disini saya akan Update'' Video, Jangan sampai ketinggalan, makanya SUBSCRIBE!!!\n" +
                    "\n" +
                    "Frost\n",
            "MiawAug's from sound of cat and dog hahaha, i like animal, and i like playing game and i'm a gamer from indonesia ^o^ check out my video while i'm gaming some games, and don't forget to subscribe.\n" +
                    "\n" +
                    "I hope you guys will enjoy my gameplay of some games :),\n" +
                    "Be a Milovers and it's make me more happy :)\n" +
                    "\n" +
                    "if like more ??? follow and like me at my social media below\n" +
                    "\n" +
                    "Email : miawaug [@] gmail.com",
            "Nama asli gw : Dyland Maximus Zidane\n" +
                    "Nama youtube gw : Dyland PROS\n" +
                    "\n" +
                    "Kenapa? ya kek gitu ga kenapa napa..\n" +
                    "\n" +
                    "Jadi gw hanya manusia yang demen main game dan suka berekspresi didepan camera.\n" +
                    "\n" +
                    "Kadang suka bikin short movie kadang maen EPEP doang tapi tetep seru lah ya.. mudahmudahan.\n" +
                    "\n" +
                    "\n" +
                    "JOIN MEMBERSHIP Channel ini biar makin seru! karna bakal dapet banyak perks special!\n" +
                    "https://www.youtube.com/channel/UCXdmo_q4SawYMz-dmeKEHPQ/join\n" +
                    "\n" +
                    "\n" +
                    "Iya iya gw cadel ga bisa ngomong R ga usah kaget2 gitu ah.\n" +
                    "\n" +
                    "Mau kenal gw lebih dalam sedalam goa emas? follow social media gw dibawah!\n" +
                    "\n" +
                    "Instagram : @dylandpros www.instagram.com/dylandpros\n" +
                    "\n" +
                    "Facebook : www.facebook.com/dylandmaximuszidane\n" +
                    "\n" +
                    "Facebook Fanpage : www.facebook.com/DylandPROS\n" +
                    "\n" +
                    "ask.fm : Dyland PROS\n" +
                    "\n" +
                    "Business , sponsorship , partnership? mail me : a.mazaya@whim.sg\n" +
                    "\n" +
                    "Like , share , comment , subscribe and as always Thanks for watching\n" +
                    "\n" +
                    "- STAY PROS -\n",
            "Welcome to My Channel\n" +
                    "\n" +
                    "my Konten : Trick & Tips / Gameplay",
            "Hello gaes kembali lagi di channel budi 01 gaming.. Ya di channel ini kelen bisa melihat betapa indahnya bacotan orangnya.. buat kelen yang lagi sakit, mulutnya berbuih tonton tonton vidio ku ini supaya apa, supaya kelen itu sembuh kembali uwooeee.\n" +
                    "\n" +
                    "jangan lupa subrek like dan comen bilang sama bapak mamak kelen bahwasannya channel ini hiburan ya uwooee..\n" +
                    "salam buriq\n",
            "Maen game bareng, seru seruan bareng, semoga terhibur dan cenel ini bermanfaat yahhh, enjoyy !!!!\n" +
                    "Contact For Business : @cahyorendy99@gmail.com\n",
            "Yogi Pramana Putra \"LetDa\" is a professional Battle Royale Player and Streamer on Youtube. He currently is playing Free Fire.\n" +
                    "\n" +
                    "*Accomplishments*\n" +
                    "- 1st Place Free Fire Creator Games 2019\n" +
                    "- 2nd Place Streamer Showdown Thailand 2019\n" +
                    "- 2nd Place City Tournament Bandung 2018\n" +
                    "- 3rd Place Influencer Free Fire Tournament 2020 \n" +
                    "\n" +
                    "He is known for his goofy energetic personality, his incredible impressions, and can often be found streaming on Youtube.\n" +
                    "\n" +
                    "Be sure to check out the official #Instagram : https://www.instagram.com/letda.hyper",
            "Kemas Pake Z kalo pake s dingin mantap kali kan nah itu aku",
            "✖ CLICK HERE TO SUBSCRIBE -https://www.youtube.com/c/erpan1140\n" +
                    "● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● \n" +
                    "\n" +
                    "● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● \n" +
                    "STEAM ACCOUNT : @ERPAN1140\n" +
                    "\n" +
                    "CONTACT :\n" +
                    "\n" +
                    "IMAIL : bijionta204@gmail.com\n" +
                    "FACEBOOK = https://www.facebook.com/adiy.irfan\n" +
                    "ASK FM = http://ask.fm/erpan1140\n" +
                    "TWITTER = https://twitter.com/AdiyIrfan\n" +
                    "INSTAGRAM:https://www.instagram.com/biji_onta/\n" +
                    "● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● \n" +
                    "✖INDONESIA\n" +
                    "✖MINECRAFT\n" +
                    "✖HORROR GAME\n" +
                    "✖CSGO\n" +
                    "✖FUN GAME\n" +
                    "● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ●"
    };

    private static final String[] link = {
            "https://www.youtube.com/c/JessNoLimit",
            "https://www.youtube.com/c/KanandaWidyantara",
            "https://www.youtube.com/user/miawaug",
            "https://www.youtube.com/c/DylandPROS",
            "https://www.youtube.com/c/FrontaLGaming",
            "https://www.youtube.com/c/BUDI01GAMING",
            "https://www.youtube.com/c/RendyRangers",
            "https://www.youtube.com/channel/UCCsHdU66sCbEtklvSVAGPsQ",
            "https://www.youtube.com/c/kemaspakezmantapkali",
            "https://www.youtube.com/c/Erpan1140"
    };

    public static ArrayList<YoutubeCh> getTopYtData() {
        ArrayList<YoutubeCh> listData = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            YoutubeCh youtubeCh = new YoutubeCh(
                    name[i],
                    subs[i],
                    photo[i],
                    description[i],
                    link[i]
            );
            listData.add(youtubeCh);
        }
        return listData;
    }
}
