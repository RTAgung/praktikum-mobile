package com.example.tugas3;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class YoutubeChAdapter extends RecyclerView.Adapter<YoutubeChAdapter.YoutubeChViewHolder> {
    private ArrayList<YoutubeCh> listData = new ArrayList<>();

    public void setData(ArrayList<YoutubeCh> listData) {
        this.listData = listData;
    }

    @NonNull
    @Override
    public YoutubeChViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        return new YoutubeChViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull YoutubeChViewHolder holder, int position) {
        holder.bind(listData.get(position));
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class YoutubeChViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView ivPhoto;
        private TextView tvName, tvSubs;

        public YoutubeChViewHolder(@NonNull View itemView) {
            super(itemView);
            ivPhoto = itemView.findViewById(R.id.iv_photo);
            tvName = itemView.findViewById(R.id.tv_name);
            tvSubs = itemView.findViewById(R.id.tv_subs);
        }

        public void bind(YoutubeCh data) {
            tvName.setText(data.getName());
            tvSubs.setText(data.getSubs());
            Glide.with(itemView)
                    .load(data.getPhoto())
                    .into(ivPhoto);

            itemView.setOnClickListener(v -> {
                Intent intent = new Intent(itemView.getContext(), DetailActivity.class);
                intent.putExtra(DetailActivity.EXTRA_YOUTUBE, data);
                itemView.getContext().startActivity(intent);
            });
        }
    }
}
