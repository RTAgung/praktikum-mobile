package com.example.tugas3;

import android.os.Parcel;
import android.os.Parcelable;

public class YoutubeCh implements Parcelable {
    private final String name;
    private final String subs;
    private final String photo;
    private final String description;
    private final String link;

    public YoutubeCh(String name, String subs, String photo, String description, String link) {
        this.name = name;
        this.subs = subs;
        this.photo = photo;
        this.description = description;
        this.link = link;
    }

    protected YoutubeCh(Parcel in) {
        name = in.readString();
        subs = in.readString();
        photo = in.readString();
        description = in.readString();
        link = in.readString();
    }

    public static final Creator<YoutubeCh> CREATOR = new Creator<YoutubeCh>() {
        @Override
        public YoutubeCh createFromParcel(Parcel in) {
            return new YoutubeCh(in);
        }

        @Override
        public YoutubeCh[] newArray(int size) {
            return new YoutubeCh[size];
        }
    };

    public String getName() {
        return name;
    }

    public String getSubs() {
        return subs;
    }

    public String getPhoto() {
        return photo;
    }

    public String getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(subs);
        parcel.writeString(photo);
        parcel.writeString(description);
        parcel.writeString(link);
    }
}
