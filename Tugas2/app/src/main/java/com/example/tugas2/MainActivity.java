package com.example.tugas2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private View balok, tabung, bola;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        balok = findViewById(R.id.view_balok);
        tabung = findViewById(R.id.view_tabung);
        bola = findViewById(R.id.view_bola);

        Button btnBalok = balok.findViewById(R.id.btn_hitung);
        Button btnTabung = tabung.findViewById(R.id.btn_hitung);
        Button btnBola = bola.findViewById(R.id.btn_hitung);

        btnBalok.setOnClickListener(view -> {
            hitungBalok();
        });
        btnTabung.setOnClickListener(view -> {
            hitungTabung();
        });
        btnBola.setOnClickListener(view -> {
            hitungBola();
        });

        showBangunRuang("balok");
    }

    private void hitungBola() {
        TextView tvLuas = bola.findViewById(R.id.tv_luas);
        TextView tvVolume = bola.findViewById(R.id.tv_volume);

        EditText etJari = bola.findViewById(R.id.et_jari);

        String strJari = etJari.getText().toString().trim();

        if (strJari.isEmpty())
            etJari.setError("field cannot be empty");
        else {
            double jari = Double.parseDouble(strJari);

            double luas = BangunHelper.hitungLuasBola(jari);
            double volume = BangunHelper.hitungVolumeBola(jari);

            tvLuas.setText("" + luas);
            tvVolume.setText("" + volume);
        }
    }

    private void hitungTabung() {
        TextView tvLuas = tabung.findViewById(R.id.tv_luas);
        TextView tvVolume = tabung.findViewById(R.id.tv_volume);

        EditText etJari = tabung.findViewById(R.id.et_jari);
        EditText etTinggi = tabung.findViewById(R.id.et_tinggi);

        String strJari = etJari.getText().toString().trim();
        String strTinggi = etTinggi.getText().toString().trim();

        if (strJari.isEmpty())
            etJari.setError("field cannot be empty");
        else if (strTinggi.isEmpty())
            etTinggi.setError("field cannot be empty");
        else {
            double jari = Double.parseDouble(strJari);
            double tinggi = Double.parseDouble(strTinggi);

            double luas = BangunHelper.hitungLuasTabung(jari, tinggi);
            double volume = BangunHelper.hitungVolumeTabung(jari, tinggi);

            tvLuas.setText("" + luas);
            tvVolume.setText("" + volume);
        }
    }

    private void hitungBalok() {
        TextView tvLuas = balok.findViewById(R.id.tv_luas);
        TextView tvVolume = balok.findViewById(R.id.tv_volume);

        EditText etPanjang = balok.findViewById(R.id.et_panjang);
        EditText etLebar = balok.findViewById(R.id.et_lebar);
        EditText etTinggi = balok.findViewById(R.id.et_tinggi);

        String strPanjang = etPanjang.getText().toString().trim();
        String strLebar = etLebar.getText().toString().trim();
        String strTinggi = etTinggi.getText().toString().trim();

        if (strPanjang.isEmpty())
            etPanjang.setError("field cannot be empty");
        else if (strLebar.isEmpty())
            etLebar.setError("field cannot be empty");
        else if (strTinggi.isEmpty())
            etTinggi.setError("field cannot be empty");
        else {
            double panjang = Double.parseDouble(strPanjang);
            double lebar = Double.parseDouble(strLebar);
            double tinggi = Double.parseDouble(strTinggi);

            double luas = BangunHelper.hitungLuasBalok(panjang, lebar, tinggi);
            double volume = BangunHelper.hitungVolumeBalok(panjang, lebar, tinggi);

            tvLuas.setText("" + luas);
            tvVolume.setText("" + volume);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.menu_balok:
                showBangunRuang("balok");
                showToast("Hitung Balok");
                item.setChecked(true);
                break;
            case R.id.menu_tabung:
                showBangunRuang("tabung");
                showToast("Hitung Tabung");
                item.setChecked(true);
                break;
            case R.id.menu_bola:
                showBangunRuang("bola");
                showToast("Hitung Bola");
                item.setChecked(true);
                break;
        }
        return true;
    }

    private void showBangunRuang(String bangun) {
        switch (bangun) {
            case "balok":
                balok.setVisibility(View.VISIBLE);
                tabung.setVisibility(View.GONE);
                bola.setVisibility(View.GONE);
                break;
            case "tabung":
                balok.setVisibility(View.GONE);
                tabung.setVisibility(View.VISIBLE);
                bola.setVisibility(View.GONE);
                break;
            case "bola":
                balok.setVisibility(View.GONE);
                tabung.setVisibility(View.GONE);
                bola.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}