package com.example.tugas2;

public class BangunHelper {

    public static double hitungLuasBalok(double panjang, double lebar, double tinggi) {
        double hasil = 2 * (panjang * lebar + panjang * tinggi + lebar * tinggi);
        hasil = Math.round(hasil * 100.0) / 100.0;
        return hasil;
    }

    public static double hitungVolumeBalok(double panjang, double lebar, double tinggi) {
        double hasil = panjang * lebar * tinggi;
        hasil = Math.round(hasil * 100.0) / 100.0;
        return hasil;
    }

    public static double hitungLuasTabung(double jari, double tinggi) {
        double hasil = 2 * Math.PI * jari * (jari + tinggi);
        hasil = Math.round(hasil * 100.0) / 100.0;
        return hasil;
    }

    public static double hitungVolumeTabung(double jari, double tinggi) {
        double hasil = Math.PI * jari * jari * tinggi;
        hasil = Math.round(hasil * 100.0) / 100.0;
        return hasil;
    }

    public static double hitungLuasBola(double jari) {
        double hasil = 4 * Math.PI * jari * jari;
        hasil = Math.round(hasil * 100.0) / 100.0;
        return hasil;
    }

    public static double hitungVolumeBola(double jari) {
        double hasil = (4 * Math.PI * jari * jari * jari) / 3;
        hasil = Math.round(hasil * 100.0) / 100.0;
        return hasil;
    }
}
