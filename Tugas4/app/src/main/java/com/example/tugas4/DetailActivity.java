package com.example.tugas4;

import androidx.appcompat.app.AppCompatActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Objects;

public class DetailActivity extends AppCompatActivity {

    public static final String EXTRA_YOUTUBE = "extra_youtube";

    private TextView tvName, tvSubs, tvDesc;
    private CircleImageView ivPhoto;
    private Button btnLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        tvName = findViewById(R.id.tv_name);
        tvSubs = findViewById(R.id.tv_subs);
        tvDesc = findViewById(R.id.tv_desc);
        ivPhoto = findViewById(R.id.iv_photo);
        btnLink = findViewById(R.id.btn_link);

        YoutubeCh youtubeCh = getIntent().getParcelableExtra(EXTRA_YOUTUBE);

        getSupportActionBar().setTitle(youtubeCh.getName());

        tvName.setText(youtubeCh.getName());
        tvSubs.setText(youtubeCh.getSubs());
        tvDesc.setText(youtubeCh.getDescription());
        Glide.with(this)
                .load(youtubeCh.getPhoto())
                .into(ivPhoto);

        btnLink.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(youtubeCh.getLink()));
            startActivity(intent);
        });
    }
}