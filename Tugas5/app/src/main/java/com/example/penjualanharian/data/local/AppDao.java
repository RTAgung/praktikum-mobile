package com.example.penjualanharian.data.local;

import com.example.penjualanharian.data.model.Penjualan;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Ignore;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface AppDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Penjualan penjualan);

    @Query("SELECT * FROM penjualan ORDER BY id DESC")
    LiveData<List<Penjualan>> getData();

    @Update
    void update(Penjualan penjualan);

    @Delete
    void delete(Penjualan penjualan);
}
