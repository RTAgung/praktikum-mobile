package com.example.penjualanharian.ui;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.example.penjualanharian.R;
import com.example.penjualanharian.data.model.Penjualan;
import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class FormActivity extends AppCompatActivity {

    private TextInputEditText etPemasukan, etPengeluaran, etTanggal;
    private Button btnForm;
    private TextView tvTitle;

    public static final String EXTRA_PENJUALAN = "extra_penjualan";
    public static final int REQUEST_ADD = 100;
    public static final int RESULT_ADD = 101;
    public static final int REQUEST_UPDATE = 200;
    public static final int RESULT_UPDATE = 201;
    public static final int RESULT_DELETE = 301;

    private boolean isEdit = false;
    private int id = 0;
    private Penjualan penjualan = null;

    final Calendar calendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        etPemasukan = findViewById(R.id.et_pemasukan_kotor);
        etPengeluaran = findViewById(R.id.et_pengeluaran);
        etTanggal = findViewById(R.id.et_tanggal);
        tvTitle = findViewById(R.id.tv_title);
        btnForm = findViewById(R.id.btn_form);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Form");

        penjualan = getIntent().getParcelableExtra(EXTRA_PENJUALAN);
        if (penjualan != null) {
            id = penjualan.getId();
            isEdit = true;
        } else
            penjualan = new Penjualan();

        String title = "";
        String btnTitle = "";

        if (isEdit) {
            title = "Edit Data";
            btnTitle = "Edit";
            if (penjualan != null) {
                etPemasukan.setText("" + penjualan.getPemasukanKotor());
                etPengeluaran.setText("" + penjualan.getPengeluaran());
                etTanggal.setText("" + penjualan.getTanggal());
            }
        } else {
            title = "Tambah Data";
            btnTitle = "Tambah";
        }

        tvTitle.setText(title);
        btnForm.setText(btnTitle);

        DatePickerDialog.OnDateSetListener date = (datePicker, year, monthOfYear, dayOfMonth) -> {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        };

        etTanggal.setOnClickListener(v -> {
            new DatePickerDialog(this, date, calendar
                    .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)).show();
        });

        btnForm.setOnClickListener(v -> {
            String sPemasukanKotor = etPemasukan.getText().toString().trim();
            String sPengeluaran = etPengeluaran.getText().toString().trim();
            String tanggal = etTanggal.getText().toString().trim();

            if (sPemasukanKotor.equals("")) etPemasukan.setError("Tidak bisa kosong!");
            else if (sPengeluaran.equals("")) etPengeluaran.setError("Tidak bisa kosong!");
            else if (tanggal.equals("")) etTanggal.setError("Tidak bisa kosong!");
            else {
                double pemasukanKotor = Double.parseDouble(sPemasukanKotor);
                double pengeluaran = Double.parseDouble(sPengeluaran);

                Penjualan penjualan = (id > 0) ? new Penjualan(
                        id,
                        pemasukanKotor,
                        pengeluaran,
                        (pemasukanKotor - pengeluaran),
                        tanggal
                ) : new Penjualan(
                        pemasukanKotor,
                        pengeluaran,
                        (pemasukanKotor - pengeluaran),
                        tanggal
                );

                Intent intent = new Intent();
                intent.putExtra(EXTRA_PENJUALAN, penjualan);

                if (isEdit) {
                    setResult(RESULT_UPDATE, intent);
                } else {
                    setResult(RESULT_ADD, intent);
                }
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateLabel() {
        String format = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);

        etTanggal.setText(sdf.format(calendar.getTime()));
    }
}