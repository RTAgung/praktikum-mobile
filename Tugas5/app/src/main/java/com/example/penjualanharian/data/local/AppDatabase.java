package com.example.penjualanharian.data.local;

import android.content.Context;

import com.example.penjualanharian.data.model.Penjualan;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Penjualan.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public abstract AppDao dao();

    public static AppDatabase initDb(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context, AppDatabase.class, "penjualan_db")
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }
}
