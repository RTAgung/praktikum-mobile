package com.example.penjualanharian.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.penjualanharian.R;
import com.example.penjualanharian.data.model.Penjualan;
import com.example.penjualanharian.ui.FormActivity;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MainViewHolder> {
    private ArrayList<Penjualan> listPenjualan = new ArrayList<>();
    private MyOnItemLongClick myOnItemLongClick;

    public void setList(ArrayList<Penjualan> listPenjualan) {
        this.listPenjualan.clear();
        this.listPenjualan = listPenjualan;
        notifyDataSetChanged();
    }

    public MainAdapter(MyOnItemLongClick myOnItemLongClick) {
        this.myOnItemLongClick = myOnItemLongClick;
    }

    @NonNull
    @Override
    public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);
        return new MainViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainViewHolder holder, int position) {
        holder.bind(listPenjualan.get(position));
    }

    @Override
    public int getItemCount() {
        return listPenjualan.size();
    }

    public class MainViewHolder extends RecyclerView.ViewHolder {
        private TextView tvPemasukanKotor, tvPengeluaran, tvPemasukanBersih, tvTanggal;
        private CardView cvItem;

        public MainViewHolder(@NonNull View itemView) {
            super(itemView);
            tvPemasukanKotor = itemView.findViewById(R.id.tv_pemasukan_kotor);
            tvPengeluaran = itemView.findViewById(R.id.tv_pengeluaran);
            tvPemasukanBersih = itemView.findViewById(R.id.tv_pemasukan_bersih);
            tvTanggal = itemView.findViewById(R.id.tv_tanggal);
            cvItem = itemView.findViewById(R.id.cv_item);
        }

        public void bind(Penjualan penjualan) {
            tvPemasukanKotor.setText(formatRupiah(penjualan.getPemasukanKotor()));
            tvPengeluaran.setText(formatRupiah(penjualan.getPengeluaran()));
            tvPemasukanBersih.setText(formatRupiah(penjualan.getPemasukanBersih()));
            tvTanggal.setText(penjualan.getTanggal());

            cvItem.setOnClickListener(v -> {
                Intent intent = new Intent(itemView.getContext(), FormActivity.class);
                intent.putExtra(FormActivity.EXTRA_PENJUALAN, penjualan);
                ((Activity) itemView.getContext()).startActivityForResult(intent, FormActivity.REQUEST_UPDATE);
            });

            cvItem.setOnLongClickListener(v -> {
                myOnItemLongClick.setOnItemLongClick(penjualan);
                return true;
            });
        }

        private String formatRupiah(double money) {
            DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
            DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

            formatRp.setCurrencySymbol(": Rp ");
            formatRp.setExponentSeparator(",");
            formatRp.setGroupingSeparator('.');

            kursIndonesia.setDecimalFormatSymbols(formatRp);
            String result = kursIndonesia.format(money);
            return result;
        }
    }

    public interface MyOnItemLongClick {
        void setOnItemLongClick(Penjualan penjualan);
    }
}
