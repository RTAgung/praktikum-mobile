package com.example.penjualanharian.data;

import com.example.penjualanharian.data.local.AppDao;
import com.example.penjualanharian.data.model.Penjualan;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.lifecycle.LiveData;

public class MainPresenter implements MainContract.presenter {
    private AppDao appDao;
    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    private MainPresenter(AppDao appDao) {
        this.appDao = appDao;
    }

    private static volatile MainPresenter instance;

    public static MainPresenter getInstance(AppDao appDao) {
        if (instance == null) {
            instance = new MainPresenter(appDao);
        }
        return instance;
    }

    @Override
    public LiveData<List<Penjualan>> getData() {
        return appDao.getData();
    }

    @Override
    public void insert(Penjualan penjualan) {
        executorService.execute(() -> {
            appDao.insert(penjualan);
        });
    }

    @Override
    public void update(Penjualan penjualan) {
        executorService.execute(() -> {
            appDao.update(penjualan);
        });
    }

    @Override
    public void delete(Penjualan penjualan) {
        executorService.execute(() -> {
            appDao.delete(penjualan);
        });
    }
}
