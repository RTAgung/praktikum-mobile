package com.example.penjualanharian.ui;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.penjualanharian.R;
import com.example.penjualanharian.data.local.AppDao;
import com.example.penjualanharian.data.local.AppDatabase;
import com.example.penjualanharian.data.model.Penjualan;
import com.example.penjualanharian.ui.adapter.MainAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private FloatingActionButton fabAdd;
    private RecyclerView rvHome;

    private MainAdapter mainAdapter;
    private MainView mainView;

    private MainAdapter.MyOnItemLongClick myOnItemLongClick = this::showDeleteDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fabAdd = findViewById(R.id.fab_add);
        rvHome = findViewById(R.id.rv_home);

        mainAdapter = new MainAdapter(myOnItemLongClick);
        mainView = MainView.getInstance(this);

        fabAdd.setOnClickListener(v -> {
            startActivityForResult(new Intent(this, FormActivity.class), FormActivity.REQUEST_ADD);
        });

        populateView();
    }

    private void populateView() {
        Observer<List<Penjualan>> observer = new Observer<List<Penjualan>>() {
            @Override
            public void onChanged(List<Penjualan> penjualan) {
                refreshData(penjualan);
            }
        };

        getData().observe(this, observer);

        rvHome.setLayoutManager(new LinearLayoutManager(this));
        rvHome.setAdapter(mainAdapter);
    }

    private void refreshData(List<Penjualan> penjualan) {
        ArrayList<Penjualan> listPenjualan = new ArrayList<>();
        listPenjualan.addAll(penjualan);
        mainAdapter.setList(listPenjualan);
    }

    private LiveData<List<Penjualan>> getData() {
        return mainView.getData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            Penjualan penjualan = data.getParcelableExtra(FormActivity.EXTRA_PENJUALAN);

            if (penjualan != null) {
                if (requestCode == FormActivity.REQUEST_ADD) {
                    if (resultCode == FormActivity.RESULT_ADD) {
                        mainView.insert(penjualan);
                        showSnackbarMessage("Data Telah Ditambah");
                    }
                } else if (requestCode == FormActivity.REQUEST_UPDATE) {
                    if (resultCode == FormActivity.RESULT_UPDATE) {
                        mainView.update(penjualan);
                        showSnackbarMessage("Data Telah Diedit");
                    }
                }
            }
        }
    }

    private void showDeleteDialog(Penjualan penjualan) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle("Hapus")
                .setMessage("Apakah ingin menghapus data ini?")
                .setCancelable(false)
                .setPositiveButton("Ya", (dialogInterface, i) -> {
                    mainView.delete(penjualan);
                    showSnackbarMessage("Data Berhasil Dihapus");
                })
                .setNegativeButton("Tidak", (dialogInterface, i) -> dialogInterface.cancel());

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void showSnackbarMessage(String message) {
        Snackbar.make(rvHome, message, Snackbar.LENGTH_SHORT).show();
    }
}