package com.example.penjualanharian.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "penjualan")
public class Penjualan implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "pemasukan_kotor")
    private double pemasukanKotor;

    @ColumnInfo(name = "pengeluaran")
    private double pengeluaran;

    @ColumnInfo(name = "pemasukan_bersih")
    private double pemasukanBersih;

    @ColumnInfo(name = "tanggal")
    private String tanggal;

    public Penjualan(int id, double pemasukanKotor, double pengeluaran, double pemasukanBersih, String tanggal) {
        this.id = id;
        this.pemasukanKotor = pemasukanKotor;
        this.pengeluaran = pengeluaran;
        this.pemasukanBersih = pemasukanBersih;
        this.tanggal = tanggal;
    }

    public Penjualan(double pemasukanKotor, double pengeluaran, double pemasukanBersih, String tanggal) {
        this.pemasukanKotor = pemasukanKotor;
        this.pengeluaran = pengeluaran;
        this.pemasukanBersih = pemasukanBersih;
        this.tanggal = tanggal;
    }

    public Penjualan() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPemasukanKotor(double pemasukanKotor) {
        this.pemasukanKotor = pemasukanKotor;
    }

    public void setPengeluaran(double pengeluaran) {
        this.pengeluaran = pengeluaran;
    }

    public void setPemasukanBersih(double pemasukanBersih) {
        this.pemasukanBersih = pemasukanBersih;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public int getId() {
        return id;
    }

    public double getPemasukanKotor() {
        return pemasukanKotor;
    }

    public double getPengeluaran() {
        return pengeluaran;
    }

    public double getPemasukanBersih() {
        return pemasukanBersih;
    }

    public String getTanggal() {
        return tanggal;
    }

    protected Penjualan(Parcel in) {
        id = in.readInt();
        pemasukanKotor = in.readDouble();
        pengeluaran = in.readDouble();
        pemasukanBersih = in.readDouble();
        tanggal = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeDouble(pemasukanKotor);
        dest.writeDouble(pengeluaran);
        dest.writeDouble(pemasukanBersih);
        dest.writeString(tanggal);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Penjualan> CREATOR = new Creator<Penjualan>() {
        @Override
        public Penjualan createFromParcel(Parcel in) {
            return new Penjualan(in);
        }

        @Override
        public Penjualan[] newArray(int size) {
            return new Penjualan[size];
        }
    };
}
