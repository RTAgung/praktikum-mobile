package com.example.penjualanharian.data;

import android.view.View;

import com.example.penjualanharian.data.model.Penjualan;

import java.util.List;

import androidx.lifecycle.LiveData;

public interface MainContract {

    interface view {
        LiveData<List<Penjualan>> getData();

        void insert(Penjualan penjualan);

        void update(Penjualan penjualan);

        void delete(Penjualan penjualan);
    }

    interface presenter {
        LiveData<List<Penjualan>> getData();

        void insert(Penjualan penjualan);

        void update(Penjualan penjualan);

        void delete(Penjualan penjualan);
    }
}
