package com.example.penjualanharian.ui;

import android.content.Context;

import com.example.penjualanharian.data.MainContract;
import com.example.penjualanharian.data.MainPresenter;
import com.example.penjualanharian.data.local.AppDao;
import com.example.penjualanharian.data.local.AppDatabase;
import com.example.penjualanharian.data.model.Penjualan;

import java.util.List;

import androidx.lifecycle.LiveData;

public class MainView implements MainContract.view {
    private MainPresenter mainPresenter;

    private MainView(MainPresenter mainPresenter) {
        this.mainPresenter = mainPresenter;
    }

    private static volatile MainView instance;

    public static MainView getInstance(Context context) {
        if (instance == null) {
            AppDao appDao = AppDatabase.initDb(context).dao();
            MainPresenter mainPresenter = MainPresenter.getInstance(appDao);
            instance = new MainView(mainPresenter);
        }
        return instance;
    }

    @Override
    public LiveData<List<Penjualan>> getData() {
        return mainPresenter.getData();
    }

    @Override
    public void insert(Penjualan penjualan) {
        mainPresenter.insert(penjualan);
    }

    @Override
    public void update(Penjualan penjualan) {
        mainPresenter.update(penjualan);
    }

    @Override
    public void delete(Penjualan penjualan) {
        mainPresenter.delete(penjualan);
    }
}
